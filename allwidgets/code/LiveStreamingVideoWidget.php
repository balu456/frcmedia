<?php

class LiveStreamingVideoWidget extends Widget {

    static $title = '';
    static $cmsTitle = 'Live Streaming Video Widget';
    static $description = 'Live Streaming Video Widget';
    static $db = array(
        'WidgetLink' => 'Text',
        'AdDropdownLink'    => 'Text',
        'UseDropdownLink'   => 'Boolean',
        'PhotoURL' => 'Text'
    );
    static $has_one = array(
        'Image' => 'Image'
    );
    static $defaults = array(
        'WidgetLink' => 'http://frmedia.org/frctv/watch-live/',
	'PhotoURL' => 'http://frmedia.org/assets/widget/live-stream.jpg'
    );

    function getCMSFields() {
        $siteconfig = SiteConfig::current_site_config();
        $folderID = $siteconfig->WidgetFolderID;
        $images = Image::get()->filter('ParentID', $folderID);
        $imagesArr = $images ? $images->map('ID', 'Name') : array();
        $preview_photo = "";
        if ($this->ImageID != 0) {
            $preview_photo = $this->Image()->CroppedImage(100, 100);
        }
        $pages = Page::get()->where("ClassName != 'ErrorPage'")->sort('Title', 'ASC');
        $pagesArr = $pages ? $pages->map('URLSegment', 'Title') : array();
        return new FieldList(
                new TextField('HeadingLine', 'Title'), 
                new DropdownField('ImageID', 'Image(280x117)', $imagesArr, $this->ImageID), 
                new LiteralField('preview_photo', $preview_photo), 
                new DropdownField('AdDropdownLink', 'Choose link', $pagesArr),
                new OptionsetField('UseDropdownLink', 'Link option', array(
                    1 => 'Use link from SiteTree',
                    0=> 'Use link from below textfield'
                )),
                new TextField('WidgetLink', 'Link')
        );
    }

}

class LiveStreamingVideoWidget_Controller extends Widget_Controller {
    
}

?>
