<?php

class JoinClassWidget extends Widget {

    static $title = '';
    static $cmsTitle = 'Join Class Widget';
    static $description = 'Ad Widget';
    static $db = array(
        'AdDescription' => 'Text',
        'AdLink' => 'Text',
        'AdDropdownLink'    => 'Text',
        'UseDropdownLink'   => 'Boolean',
        'PhotoURL' => 'Text',
        'IconURL' => 'Text'
    );
    static $has_one = array(
        'Photo' => 'Image',
        'Icon' => 'Image'
    );
    static $defaults = array(
        'PhotoID' => 0,
        'IconID' => 0,
        'AdDescription' => 'We are looking for volunteers to produce local television or radio programs. Interested? Click "Learn More" to contact us about how you can get involved.',
        'AdLink' => 'http://frmedia.org/frcmedia-sign-up-form'
    );

    function getCMSFields() {
        $siteconfig = SiteConfig::current_site_config();
        $folderID = $siteconfig->WidgetFolderID;
        $images = Image::get()->filter('ParentID', $folderID);
        $imagesArr = $images ? $images->map('ID', 'Name') : array();
        
        $pages = Page::get()->where("ClassName != 'ErrorPage'")->sort('Title ASC');
        $pagesArr = $pages ? $pages->map('URLSegment', 'Title') : array();
        $preview_photo = "";
        $preview_icon = "";
        if ($this->PhotoID != 0) {
                $preview_photo = $this->Photo()->CroppedImage(100, 100);
        }if ($this->IconID != 0) {
                $preview_icon = $this->Icon()->CroppedImage(100, 100);
        }
        return new FieldList(
                        new DropdownField('PhotoID', 'Photo(259x119)', $imagesArr, $this->PhotoID),
                        new LiteralField('preview_photo', $preview_photo),
                        new DropdownField('IconID', 'Icon(97x97)', $imagesArr, $this->IconID),
                        new LiteralField('preview_icon', $preview_icon),
                        new DropdownField('AdDropdownLink', 'Choose link', $pagesArr),
                        new OptionsetField('UseDropdownLink', 'Link option', array(
                            1 => 'Use link from SiteTree',
                            0 => 'Use link from below textfield'
                        )),
                        new TextField('AdLink', 'Type link'),
                        new TextareaField('AdDescription', 'Description')
        );
    }

}

class JoinClassWidget_Controller extends Widget_Controller {
    
}

?>