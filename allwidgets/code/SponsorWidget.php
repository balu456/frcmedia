<?php

class SponsorWidget extends Widget {

        static $title = '';
        static $cmsTitle = 'Sponsor widget';
        static $description = 'Get all sponsors';
        static $db = array(
            'Headingline' => 'Varchar(255)'
        );
        static $has_one = array(
        );
        static $defaults = array(
            'Headingline' => 'Visit our Sponsor'
        );
        function getCMSFields() {
                return new FieldList(
                                new TextField('Headingline', 'Heading line')
                );
        }

}

class SponsorWidget_Controller extends Widget_Controller {

        function init() {
                parent::init();
                Requirements::javascript(Director::absoluteURL('themes/default/js/jquery.anythingslider.js'));
        }

        function Sponsors() {
                return Sponsor::get();
        }

}

?>
