<?php

class FRCTVChannelGuideWidget extends Widget {

    static $title = '';
    static $cmsTitle = 'FRCTV Channel Guide';
    static $description = 'FRCTV Channel Guide';
    static $db = array(
    );
    static $has_one = array(
    );
    static $defaults = array(
    );

    function Title() {
        return $this->WidgetTitle ? $this->WidgetTitle : self::$title;
    }

}

class FRCTVChannelGuideWidget_Controller extends Widget_Controller {

    function generateTemplate() {
        $schedulePage = DataObject::get_one('FRCTVProgrammingSchedulePage');
        return $this->customise(array(
                    'Schedules' => API::TVSchedules(5, $date = false, $startAM = false, $endAM = true),
                    'SchedulesLink' => $schedulePage ? $schedulePage->Link() : '#',
                    'Date' => date('l, F jS')
                        ))
                        ->renderWith('ScheduleTV');
    }

}

?>
