<?php

class SingleContentWidget extends Widget {

    static $title = '';
    static $cmsTitle = 'AD widget';
    static $description = 'Ads manager';
    static $db = array(
        'Name' => 'Varchar(255)',
        'AdDescription'   => 'Text',
        'AdLink' => 'Text'
    );
    static $has_one = array(
        'Photo' => 'Image',
        'Icon' => 'Image'
    );
    static $defaults = array(
        'SinglePage' => 'home'
    );

    function Title() {
        return $this->WidgetTitle ? $this->WidgetTitle : self::$title;
    }

    function getCMSFields() {
        // TODO: Usar TreeDropdownField when bug fixed 
        $images = Image::get();
        $imagesArr = $images ? $images->map('ID', 'Name') : array();
        $preview_photo = "";
        $preview_icon = "";
        if($this->PhotoID != 0){
            $preview_photo = $this->Photo()->CroppedImage(100, 100);
        }if($this->IconID != 0){
            $preview_icon = $this->Icon()->CroppedImage(100, 100);
        }
        return new FieldList(
                new TextField('Name', 'Name'), 
                new DropdownField('PhotoID', 'Photo', $imagesArr),
                new LiteralField('preview_photo', $preview_photo),
                new DropdownField('IconID', 'Icon', $imagesArr),
                new LiteralField('preview_icon', $preview_icon),
                new TextField('AdLink', 'Ad Link'),
                new TextareaField('AdDescription', 'Description')
        );
    }

}

?>
