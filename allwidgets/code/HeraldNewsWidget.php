<?php

class HeraldNewsWidget extends Widget {

    static $title = '';
    static $cmsTitle = 'Herald News';
    static $description = 'News from Herald page';
    static $db = array(
    );
    static $has_one = array(
    );
    static $defaults = array(
    );

    function Title() {
        return $this->WidgetTitle ? $this->WidgetTitle : self::$title;
    }

}

class HeraldNewsWidget_Controller extends Widget_Controller {
    
    function xml_attribute($object, $attribute)
    {
        if(isset($object[$attribute]))
            return (string) $object[$attribute];
    }
    
    function getRecurseValues($xml, &$data, $parent = "") {
        $conv_value = "";
        $child_count = 0;
        foreach ($xml as $key => $value) {
            $child_count++;
            $k = ($parent == "") ? (string) $key : $parent . "_" . (string) $key;
            if ($this->getRecurseValues($value, $data, $k) == 0) {  // no childern, aka "leaf node"
                if ($k == 'link') {                    
                    $conv_value = (string)$value;
                }
                else
                    $conv_value = Convert::raw2xml($value);
            }
            if (array_key_exists($k, $data) == true) {
                $data[$k] = $data[$k] . "," . $conv_value;
            } else {
                $data[$k] = $conv_value;
            }
        }
        return $child_count;
    } 

    public function HeraldNews() {
        $url = 'http://www.heraldnews.com/news/rss';
		$url = 'http://www.heraldnews.com/news?template=rss&mime=xml';
        $rss = new RestfulService($url);
        $xml = $rss->request()->getBody();
        if(!($element = @simplexml_load_string($xml)))
                return false;
        $output = new ArrayList();
		//print_r($element);die;
        $childElements = $element->{'channel'}->{'item'};
        if ($childElements) {
            foreach ($childElements as $child) {
                $data = array();
                $this->getRecurseValues($child, $data);
                $output->push(new ArrayData($data));
            }
        }
        return $output->limit(5);
    }

}

?>
