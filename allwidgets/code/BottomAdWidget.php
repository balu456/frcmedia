<?php

class BottomAdWidget extends Widget {

    static $title = '';
    static $cmsTitle = 'Bottom Ad';
    static $description = 'Bottom Ad Widget';
    static $db = array(
        'AdLink' => 'Text',
        'AdDropdownLink'    => 'Text',
        'UseDropdownLink'   => 'Boolean',
        'PhotoURL' => 'Text'
    );
    static $has_one = array(
        'Photo' => 'Image'
    );
    static $defaults = array(
    );

    function getCMSFields() {
        $siteconfig = SiteConfig::current_site_config();
        $folderID = $siteconfig->WidgetFolderID;
        $images = Image::get()->filter('ParentID', $folderID);
        $imagesArr = $images ? $images->map('ID', 'Name') : array();
        
        $pages = Page::get()->where("ClassName != 'ErrorPage'")->sort('Title ASC');
        $pagesArr = $pages ? $pages->map('URLSegment', 'Title') : array();
        $preview_photo = "";
        if ($this->PhotoID != 0) {
                $preview_photo = $this->Photo()->CroppedImage(100, 100);
        }
        return new FieldList(
                        new DropdownField('PhotoID', 'Photo(291x70)', $imagesArr),
                        new LiteralField('preview_photo', $preview_photo),
                        new DropdownField('AdDropdownLink', 'Choose link', $pagesArr),
                        new OptionsetField('UseDropdownLink', 'Link option', array(
                            1 => 'Use link from SiteTree',
                            0=> 'Use link from below textfield'
                        )),
                        new TextField('AdLink', 'Type link')
        );
    }

}

class BottomAdWidget_Controller extends Widget_Controller {
    
}

?>
