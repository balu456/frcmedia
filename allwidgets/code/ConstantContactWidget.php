<?php

class ConstantContactWidget extends Widget {

    static $title = '';
    static $cmsTitle = 'Constant Contact';
    static $description = 'Constant Contact Form';
    static $db = array(
    );
    static $has_one = array(
    );
    static $defaults = array(
    );

    function Title() {
        return $this->WidgetTitle ? $this->WidgetTitle : self::$title;
    }

}

?>
