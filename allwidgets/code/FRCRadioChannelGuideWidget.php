<?php

class FRCRadioChannelGuideWidget extends Widget {

        static $title = '';
        static $cmsTitle = 'FRCRadio Channel Guide';
        static $description = 'FRCRadio Channel Guide';
        static $db = array(
                );
        static $has_one = array(
                );
        static $defaults = array(
                );

        function Title() {
                return $this->WidgetTitle ? $this->WidgetTitle : self::$title;
        }

}

class FRCRadioChannelGuideWidget_Controller extends Widget_Controller {

        function generateTemplate() {
                $schedulePage = RadioProgrammingSchedulePage::get()->First();
                return $this->customise(array(
                                    'Schedules' => GoogleCalendar::getEventsByDate(null,
                                            5),
                                    'SchedulesLink' => $schedulePage ? $schedulePage->Link() : '#',
                                    'Date' => date('l, F jS')
                                ))
                                ->renderWith('ScheduleRadio');
        }

}

?>
