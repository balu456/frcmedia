<?php

class LiveStreamingRadioWidget extends Widget {

    static $title = '';
    static $cmsTitle = 'Live Streaming Radio Widget';
    static $description = 'Live Streaming Radio Widget';
    static $db = array(
        'PhotoURL' => 'Text',
    );
    static $has_one = array(
        'Image' => 'Image'
    );
    static $defaults = array(
    );

    function getCMSFields() {
        $siteconfig = SiteConfig::current_site_config();
        $folderID = $siteconfig->WidgetFolderID;
        $images = Image::get()->filter('ParentID', $folderID);
        $imagesArr = $images ? $images->map('ID', 'Name') : array();
        $preview_photo = "";
        if ($this->ImageID != 0) {
            $preview_photo = $this->Image()->CroppedImage(100, 100);
        }
        return new FieldList(
                new DropdownField('ImageID', 'Image(280x117)', $imagesArr, $this->ImageID), 
                new LiteralField('preview_photo', $preview_photo)
        );
    }

}

class LiveStreamingRadioWidget_Controller extends Widget_Controller {

    function popup() {
        return $this->renderWith('RadioPopup');
    }

}

?>
