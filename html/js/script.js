(function($) {
    $(document).ready(function(){
		/* Header - Search focus */
		var clearMePrevious = 'Search FRMedia.org';
		// clear input on focus
		$('#SearchForm_SearchForm_Search').focus(function(){
			if($(this).val()=="Search FRMedia.org"){
				clearMePrevious = $(this).val();
				$(this).val('');
			}
		});
		// if field is empty afterward, add text again
		$('#SearchForm_SearchForm_Search').blur(function(){
			if($(this).val()==''){
				$(this).val(clearMePrevious);
			}
		});
		
		/* Homepage - Top slider */		
		var scPlaying = [];
        $('#slider1').anythingSlider({
			hashTags            : false,
			theme           : 'metallic',
			easing          : 'easeInOutBack',
			resizeContents      : true,
			addWmodeToObject    : 'transparent',
			buildArrows         : true,
			buildNavigation     : false,  
			buildStartStop      : false,
			enableNavigation    : false, 
			enableStartStop     : false, 
			enableKeyboard      : false, 
			autoPlay            : true,
			autoPlayLocked      : true,
			autoPlayDelayed     : true,
			delay               : 3000,
			resumeDelay         : 3600,
			animationTime       : 600,
			delayBeforeAnimate  : 0
		});
		
		/* Home - bottom slider */
		$('#spon').anythingSlider({
			hashTags            : false,
			buildArrows         : false,
			buildNavigation     : false,
			buildStartStop      : false,
			enableArrows        : false,
			enableNavigation    : false,
			enableStartStop     : false,
			enableKeyboard      : false, 
			autoPlay            : true,
			autoPlayLocked      : true,
			autoPlayDelayed     : true,
			delay               : 3000,
			resumeDelay         : 3000,
			animationTime       : 600,
			delayBeforeAnimate  : 0
		});
		
		/* Home - Tab control */
		$(".tab_content").hide();
		$(".tab_content:first").show(); 
	
		$("ul.tabs li").click(function() {
			$("ul.tabs li").removeClass("active");
			$(this).addClass("active");
			$(".tab_content").hide();
			var activeTab = $(this).attr("rel"); 
			$("#"+activeTab).fadeIn(); 
		});
		
		/* Gallery */
		$('#gallery a').lightBox();
    })
})(jQuery);