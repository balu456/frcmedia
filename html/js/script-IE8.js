(function($) {
    $(document).ready(function(){
        $('.FooterContainer').corner("bottom 20px");
		$('.rss').corner("15px");
		$('.article-item').corner("20px");
		$('.slider').corner("20px");
		$('.anythingSlider-metallic').corner("20px");
		$('.slider-content .blockText').corner("15px");
		$('.slider-link').corner("7px");
		$('.block .block-title').corner("top 15px");
		$('.block .block-content').corner("bottom 15px");
		$('.tab_container').corner("bottom 15px");
		$('.joinclass').corner("20px");
		$('.visitspon .visit-title').corner("top 10px");
		$('.about-mission').corner("15px");
		$('input[type=submit]').corner("5px");
		$('.program').corner("15px");
    })
})(jQuery);