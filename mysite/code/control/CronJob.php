<?php

class Cronjob_Controller extends Page_Controller {

        public static $allowed_actions = array(
            'updateprogramtvpage'
        );

        public function __construct() {
                parent::__construct();
        }

        public function index() {
                Controller::curr()->redirect('/');
        }

        public function updateprogramtvpage() {
                $_GET['stage'] = 'Stage'; //set Request to get data from Stage Table
                $programTVPages = TVProgramPage::get();
                if ($programTVPages) {
                        foreach ($programTVPages as $program) {
                                $program->processAPIImport($isImportFormCronjob = true);
                        }
                        echo "done";
                }
                return;
        }
        
//        function processAPIImport() {
//                if (!(int) $this->ProjectID)
//                        return false;
//                $allShows = API::getAllShowsByProjectID($this->ProjectID);
//                $arrVideosPage = VideoOnDemandPage::get()->map('ShowID')->toArray();
////                                print_r($arrVideosPage);die;
//                foreach ($allShows as $show) {
//                        if (array_key_exists($show->ShowID, $arrVideosPage)) {
//                                echo "if<br>";
//                                $videoOndemandPage = VideoOnDemandPage::get()->filter(array(
//                                            "ShowID" => $show->ShowID))->first();
//                                if ($videoOndemandPage) {//print_r($videoOndemandPage);die;
//                                        $videoOndemandPage->StreamingFileURL = $show->StreamingFileURL;
//                                        $videoOndemandPage->write();
//                                }
//                        } else {
//                                echo "else<br>";
//                                $videoData = array(
//                                    'ParentID' => $this->ID,
//                                    'Title' => $show->Title,
//                                    'StreamingFileURL' => $show->StreamingFileURL,
//                                    'ShowID' => $show->ShowID,
//                                    'VideoType' => TIGHTROPE
//                                );
//                                $videoOndemandPage = new VideoOnDemandPage($videoData);
//                                $videoOndemandPage->writeToStage('Stage');
//                                $videoOndemandPage->doPublish();
//                        }
//                        if (trim($videoOndemandPage->StreamingFileURL) == '') {
//                                echo "streaming null: {$videoOndemandPage->ID}<br>";
//                                if ($videoOndemandPage->getExistsOnLive())
//                                        $videoOndemandPage->doDeleteFromLive();
////                                $videoOndemandPage->doUnpublish();
////                                
//                        }
//                        else if (!$videoOndemandPage->isPublished()) {
////                                $videoOndemandPage->writeToStage('Stage');
////                                $videoOndemandPage->publish('Stage','Live');
////                                $videoOndemandPage->publish('Stage', 'Live');
//                                echo "streaming save publish<br>";
//                        }
////                        if ($videoOndemandPage->getIsDeletedFromStage() && $videoOndemandPage->getExistsOnLive()) {
//////                                $videoOndemandPage->delete();
////                                echo "Deleted {$videoOndemandPage->ID}";
////                        }
//                }
//        }

}

?>
