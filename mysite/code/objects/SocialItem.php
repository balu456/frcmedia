<?php

class SocialItem extends DataObject {

    static $db = array(
        'Title' => 'Text',
        'Link' => 'Text'
    );
    static $has_one = array(
        'Icon' => 'Image'
    );

    function getCMSFields() {
        return new FieldList(
                new TextField('Title'), new TextField('Link'), new UploadField('Icon', 'Icon(31x32)')
        );
    }

    function thumb() {
        return $this->IconID != 0 ? $this->Icon()->CroppedImage(31, 32) : 'no-image';
    }

    static function summaryTitleFields() {
        return array(
            'Title' => 'Title',
            'Link' => 'Link',
            'thumb' => 'Icon'
        );
    }
}

?>
