<?php

class PhotoGalleryAlbum extends DataObject {

    static $db = array(
        'Title' => 'Varchar(255)',
        'Description' => 'HTMLText',
    );
    static $has_one = array(
        'CoverImage' => 'Image',
        'GalleryPage' => 'PhotoGalleryPage'
    );
    static $has_many = array(
        'GalleryItems' => 'PhotoGalleryItem'
    );
    static $defaults = array(
    );
    public static $summary_fields = array(
        'Title' => 'Title',
        'Thumbnail' => 'Thumbnail'
    );

    function getCMSFields() {
        if ($this->ID == 0) {
            $this->GalleryPageID = Session::get('GalleryPageID');
            $this->write();
        }
        Session::set('GalleryAlbumID', $this->ID);
        $fields = new FieldList(new TabSet('Root'));
        $fields->findOrMakeTab('Root.Main', 'Main');
        $fields->addFieldsToTab('Root.Main', array(
            new UploadField('CoverImage', 'Image(Any size ratio with 113x88)'),
            new TextField('Title', 'Title'),
            new HtmlEditorField('Description', 'Description'),
        ));
        $fields->findOrMakeTab('Root.Photos', 'Photos');
        $config = GridFieldConfig_RecordEditor::create();
        $config->addComponent(new GridFieldBulkEditingTools());
        $config->addComponent(new GridFieldBulkImageUpload());
        $photoGF = new GridField(
                'Photos', 'Photos', $this->GalleryItems(), $config
        );
        $fields->addFieldToTab('Root.Photos', $photoGF);

        return $fields;
    }

    public function getThumbnail() {
        return $this->CoverImageID != 0 ? $this->CoverImage()->CMSThumbnail() : 'no-image';
    }

    public function Link() {
        return $this->GalleryPage()->Link('album/' . $this->ID . '-' . $this->Title);
    }

}

?>
