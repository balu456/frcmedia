<?php

class Sponsor extends DataObject {

    static $db = array(
        'Name' => 'Varchar(255)',
        'Link' => 'Varchar(255)',
        'SortOrder' => 'Int'
    );
    static $has_one = array(
        'Image' => 'Image'
    );
    
    public static $default_sort = 'SortOrder';

    function thumb() {
        return $this->ImageID != 0 ? $this->Image()->CMSThumbnail() : 'No image';
    }

    static $summary_fields = array(
        'Name' => 'Name',
        'Link' => 'Link',
        'thumb' => 'Image',
    );

    function getCMSFields() {
        return new FieldList(
                new TextField('Name', 'Name'),
                new TextField('Link', 'Link'),
                new UploadField('Image', 'Image(235x101)')
            );
    }

    function customSize() {
        if ($this->ImageID != 0) {
            return $this->Image()->getHeight() > 60 ? $this->Image()->SetHeight(60) : $this->Image();
        }
    }

}

?>
