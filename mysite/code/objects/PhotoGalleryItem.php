<?php

class PhotoGalleryItem extends DataObject {

    static $db = array(
        'Title' => 'Varchar(255)',
        'Description' => 'HTMLText'
    );
    static $has_one = array(
        'Image' => 'Image',
        'GalleryAlbum' => 'PhotoGalleryAlbum',
        'GalleryPage' => 'PhotoGalleryPage'
    );
    static $defaults = array(
    );
    public static $summary_fields = array(
        'Title' => 'Title',
        'Thumbnail' => 'Thumbnail'
    );

    function getCMSFields() {
        if ($this->ID == 0) {
            $this->GalleryAlbumID = Session::get('GalleryAlbumID');
            $this->GalleryPageID = Session::get('GalleryPageID');
            $this->write();
        }
        $fields = new FieldList(new TabSet('Root'));
        $fields->findOrMakeTab('Root.Main', 'Main');
        $fields->addFieldsToTab('Root.Main', array(
            new UploadField('Image', 'Image(197x155)'),
            new TextField('Title', 'Title'),
            new HtmlEditorField('Description', 'Description')
        ));        
        return $fields;
    }

    public function getThumbnail() {
        return $this->ImageID != 0 ? $this->Image()->CMSThumbnail() : 'no-image';
    }

}

?>
