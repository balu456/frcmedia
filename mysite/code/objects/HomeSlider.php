<?php
class HomeSlider extends DataObject{
    static $db = array(
        'SliderType'  => 'Varchar',
        
        // Article Story
        'ArticleStoryTitle' => 'Varchar(255)',
        'ArticleStoryDesc'  => 'HTMLText',
        'ArticleStoryLink'  => 'Text',
        'ArticleStoryButtonLink'    => 'Varchar',
        
        // Live Streaming Video
        'LiveStreamingVideoTitle' => 'Varchar(255)',
        'LiveStreamingVideoDesc'    => 'HTMLText',
        'LiveStreamingVideoButtonLink'    => 'Text',
        'LiveStreamingVideoLink'  => 'Text',
        'UseEmbedVideo' => 'Boolean',
        'EmbedVideo'    => 'HTMLText',
        
        // Live Streaming Radio
        'LiveStreamingRadioTitle' => 'Varchar(255)',
        'LiveStreamingRadioDesc'    => 'HTMLText',
        'LiveStreamingRadioButtonLink'    => 'Text',
        'LiveStreamingRadioLink'  => 'Text',
        
        'SortOrder' => 'Int'
    );
    static $has_one = array(
        'ArticleStoryImage' => 'Image',
        'HomePage'  => 'HomePage',
        'Page'  => 'Page'
    );
    
    public static $default_sort = 'SortOrder';
    
    static $summary_fields = array(        
        'revertTitle'   => 'Title',
        'SliderType' => 'Slider Type'
    );
    
    function revertTitle(){
        if($this->SliderType == 'ArticleStory')
            return $this->ArticleStoryTitle;
        else if($this->SliderType == 'LiveStreamingVideo')
            return $this->LiveStreamingVideoTitle;
        else if($this->SliderType == 'LiveStreamingRadio')
            return $this->LiveStreamingRadioTitle;
    }
    
    function getCMSFields() {
        $fields = new FieldList(new TabSet('Root'));
        $typeArr = array(
            'ArticleStory'  => 'Article Story',
            'LiveStreamingVideo'  => 'Live Streaming Video',
            'LiveStreamingRadio'  => 'Live Streaming Radio',
        );
        
        $fields->findOrMakeTab('Root.Config', 'Config');
        $fields->addFieldsToTab('Root.Config', array(
            new DropdownField('SliderType', 'Slider type', $typeArr)
        ));
        $fields->findOrMakeTab('Root.ArticleStory', 'Article Story');
        $fields->addFieldsToTab('Root.ArticleStory', array(
            new TextField('ArticleStoryTitle', 'Title'),
            new HtmlEditorField('ArticleStoryDesc', 'Left description'),
            new UploadField('ArticleStoryImage', 'Image(458x271)'),
            new TextField('ArticleStoryButtonLink', 'Text in button'),
            new TreeDropdownField('PageID', 'Link', 'SiteTree', 'ID', 'MenuTitle'),
        ));
        $fields->findOrMakeTab('Root.LiveStreamingVideo', 'Live Streaming Video');
        $fields->addFieldsToTab('Root.LiveStreamingVideo', array(
            new TextField('LiveStreamingVideoTitle', 'Title'),
            new HtmlEditorField('LiveStreamingVideoDesc', 'Left description'),
            new TextField('LiveStreamingVideoButtonLink', 'Text in button'),
            new TreeDropdownField('LiveStreamingVideoLink', 'Link', 'SiteTree', 'URLSegment', 'Title'),
            new CheckboxField('UseEmbedVideo', 'Use embed video ?'),
            new TextareaField('EmbedVideo', 'Embed code')
        ));
        $fields->findOrMakeTab('Root.LiveStreamingRadio', 'Live Streaming Radio');
        $fields->addFieldsToTab('Root.LiveStreamingRadio', array(
            new TextField('LiveStreamingRadioTitle', 'Title'),
            new HtmlEditorField('LiveStreamingRadioDesc', 'Left description'),
            new TextField('LiveStreamingRadioButtonLink', 'Text in button'),
            new TreeDropdownField('LiveStreamingRadioLink', 'Link', 'SiteTree', 'URLSegment', 'Title'),
        ));
                
        return $fields;
    }
    
    function renderHomeRadio(){
        $radioWidgetController = new LiveStreamingRadioWidget_Controller();
        return $radioWidgetController->popup();
    }
    
    function ArticleLink(){
            if($id = $this->LinkID){
                    $page = DataObject::get_by_id('ArticlePage', $id);
                    return $page ? $page->Link() : '#';
            }
            return '#';
    }
}
?>
