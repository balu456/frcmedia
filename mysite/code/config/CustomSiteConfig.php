<?php

class CustomSiteConfig extends DataExtension {

    static $db = array(
        'Footer' => 'HTMLText',
        'NumberItemFeed' => 'Int',
        'TwitterMain' => 'HTMLText',
        'TwitterSearch' => 'HTMLText',
        'FacebookPlugin' => 'HTMLText',
    );
    static $has_one = array(
        'HomeLogo' => 'Image',
        'Logo' => 'Image',
        'WidgetFolder'  => 'Folder'
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->findOrMakeTab('Root.Main');
        $fields->addFieldsToTab('Root.Main', array(
            new HeaderField('HomeLogoHeader', 'Logo'),
            new UploadField('HomeLogo', 'Home Logo(240x75)')
        ));

        $fields->findOrMakeTab('Root.Footer', 'Footer');
        $fields->addFieldsToTab('Root.Footer', array(
            new HtmlEditorField('Footer', 'Footer'),
        ));
        $conf = GridFieldConfig_RelationEditor::create(10);
        $conf->addComponent(new GridFieldSortableRows('SortOrder'));
        $fields->findOrMakeTab('Root.Sponsor', 'Sponsor');
        $fields->addFieldsToTab('Root.Sponsor', array(
            new GridField(
                    'Sponsor', 'Sponsors', Sponsor::get(), $conf
            )
        ));
        $fields->findOrMakeTab('Root.Social', 'Social Connect');
        $fields->addFieldsToTab('Root.Social', array(
            new HeaderField('ChannelGuide', 'FRCTV Channel Guide - RSS Feed'),
            new NumericField('NumberItemFeed', 'Number items show'),
            new HeaderField('SocialScripts', 'Social Scripts'),
            new TextareaField('TwitterMain', "Twitter main"),
            new TextareaField('TwitterSearch', "Twitter search"),
            new TextareaField('FacebookPlugin', "Facebook plugin"),
            new HeaderField('Socialitems', 'Social icons'),
            new GridField(
                    'SocialItem', 'SocialItems', SocialItem::get(), GridFieldConfig_RecordEditor::create()
            )
        ));
        $folders = Folder::get();
        $foldersArr = $folders ? $folders->map('ID', 'Title') : array();
        $fields->findOrMakeTab('Root.WidgetConfig', 'Widget config');
        $fields->addFieldsToTab('Root.WidgetConfig', array(
            new DropdownField('WidgetFolderID', 'Choose images folder for all widgets', $foldersArr)
        ));
    }
}
?>
