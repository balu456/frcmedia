<?php

Class Calendar_ControllerDeco extends DataExtension {

    public function PaginatedPages() {
        $data = $this->owner->data()->getEventList(
                $this->owner->getStartDate()->date(), $this->owner->getStartDate()->date()
        );
        $events = new DataList('CalendarDateTime');
        $events->addMany($data);
        $pagingList = new PaginatedList($events, $this->owner->request);
        return $pagingList->setPageLength($this->owner->EventsPerPage);
    }

}

?>
