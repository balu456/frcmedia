<?php

Class CalendarEventDeco extends DataExtension {

    static $db = array(
        'Location'  => 'Varchar(255)',
        'Longitude' => 'Varchar(255)',
        'Latitude' => 'Varchar(255)',
    );
    
    static $has_one = array(
        'Image' => 'Image'
    );

    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab('Root.Main', array(
            new TextField('Location', 'Location'),
            new UploadField('Image', 'Image(162x127)')
            ), 'Content');
        $fields->findOrMakeTab('Root.Map', 'Map');
        $fields->addFieldsToTab('Root.Map', array(
            new TextField('Longitude', 'Longitude'),
            new TextField('Latitude', 'Latitude')
        ));
    }

}

?>
