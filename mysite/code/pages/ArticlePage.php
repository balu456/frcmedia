<?php

class ArticlePage extends Page {

        static $db = array(
        );
        static $has_one = array(
        );
        static $defaults = array(
        );

        function getCMSFields() {
                $fields = parent::getCMSFields();
                return $fields;
        }
        
        function URL(){
                return Director::absoluteURL($this->Link());
        }

}

class ArticlePage_Controller extends Page_Controller {
}

?>
