<?php

class FRCTVProgrammingSchedulePage extends Page {
    
}

class FRCTVProgrammingSchedulePage_Controller extends Page_Controller {

    function create7days() {
        $results = new ArrayList();
        $today = date('F j');
        $date = null;
        $todayCheck = date("Y-m-d");
        if(isset($_GET['date']))
            $date = $_GET['date'];
        $arrayData = new ArrayData(array(
            'ProgramDate' => $today,
            'Link'  => $this->Link('?date='.date("Y-m-d")),
            'LinkingMode' => $date ? ($todayCheck == $date ? 'current' : 'normal') : 'current'
                )
        );
        $results->add($arrayData);
        for ($i = 1; $i <= 6; $i++) {
            $dayCheck = date('Y-m-d', strtotime('+' . $i . 'days'));
            $arrayData = new ArrayData(array(
                'ProgramDate' => date('F j', strtotime('+' . $i . 'days')),
                'Link'  => $this->Link('?date='.date("Y-m-d", strtotime('+' . $i . 'days'))),
                'LinkingMode' => $dayCheck == $date ? 'current' : 'normal'
                    )
            );
            $results->add($arrayData);
        }
        return $results;
    }
    
    function getProgramsByDate(){
        if(isset($_GET['date'])){
            $date = $_GET['date'];
            return API::TVSchedules(false, $date, $startAM = true);
        }
        else{
            return API::TVSchedules($limit = false, $date = false, $startAM = true);
        }
        
    }

}

?>
