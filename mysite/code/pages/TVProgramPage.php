<?php

class TVProgramPage extends Page {

    static $db = array(
        'FirstLine' => 'Varchar(255)',
        'ProjectID' => 'Int'
    );
    static $has_one = array(
        'TopImage' => 'Image',
        'VideoThumbnail' => 'Image'
    );
    static $defaults = array(
    );
    static $allowed_children = array(
        'VideoOnDemandPage',
        'VideoArchivePage'
    );
    static $widgets_default = array(
        'FRCTVChannelGuideWidget',
        'LiveStreamingVideoWidget',
        'ConstantContactWidget',
        'JoinClassWidget',
        'SponsorWidget',
    );

    function onBeforeWrite() {
        parent::onBeforeWrite();

        if (isset($_POST['Update']) && $this->ProjectID) {
            if (trim($this->Content) == '') {
                $project = API::getProjectByID($this->ProjectID);
                if ($project) {
                    $this->Content = $project->Description;
                }
            }
        }
    }

    function onAfterWrite() {
        parent::onAfterWrite();
        if (!$this->SideBarID) {
            $widgetarea = new WidgetArea();
            $widgetarea->write();
            $this->SideBarID = $widgetarea->ID;
            $widgets = self::$widgets_default;
            if (!empty($widgets)) {
                foreach ($widgets as $widget) {
                    $widgetObj = new $widget();
                    $widgetObj->ParentID = $this->SideBarID;
                    $widgetObj->write();
                }
            }
            $this->write();
        }
        if (isset($_POST['Update']) && $this->ProjectID) {
            $this->processAPIImport();
        }
        $this->doCreatedArchivePage();
    }

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', new NumericField('ProjectID', 'Project ID'), 'Content');
        $fields->addFieldToTab('Root.Main', new CheckboxField('Update', 'Update from Tightrope', false), 'Content');
        $fields->addFieldToTab('Root.Main', new TextField('FirstLine', 'First line above image'), 'Content');
        $fields->findOrMakeTab('Root.TopImage', 'Top image');
        $fields->addFieldToTab('Root.TopImage', new UploadField('TopImage', 'Top image(536x141)'));

        $fields->findOrMakeTab('Root.VideoThumbnail', 'Video Thumbnail');
        $fields->addFieldToTab('Root.VideoThumbnail', new UploadField('VideoThumbnail', 'Default video thumbnail image(125x75)'));
        return $fields;
    }

    function AllVideos($limit = null) {
        $videos = VideoOnDemandPage::get()->filter(array('ParentID' => $this->ID))->sort('ShowID', 'DESC');
        if ($videos) {
            foreach ($videos as $video) {
                if (!$video->isPublished()) {
                    $videos->remove($video);
                }
            }			
            $results = $videos->limit($limit);
			return $results;
        }
        return false;
    }

    function revertTopImage() {
        if ($this->TopImageID != 0) {
            return $this->TopImage()->getWidth() > 536 ? $this->TopImage()->SetWidth(536) : $this->TopImage();
        }
    }

    function archivePage() {
        $pages = VideoArchivePage::get()->filter(array(
            "TVProgramPageID" => $this->ID));
        return $pages ? $pages->First() : false;
    }

    function doCreatedArchivePage() {
        if (!$this->archivePage()) {
            $videoArchivePage = new VideoArchivePage();
            $videoArchivePage->Title = 'Video Archive';
            //$videoArchivePage->URLSegment = 'video-archive-' . $this->ID;
            $videoArchivePage->TVProgramPageID = $this->ID;
            $videoArchivePage->ParentID = $this->ID;
            $videoArchivePage->write();
            $videoArchivePage->publish('Stage', 'Live');
        }
    }

    function getArchivePageLink() {
        $page = $this->archivePage();
        return $page ? $page->Link() : '#';
    }

    function processAPIImport($isImportFromCronjob = false) {
        $allShows = API::getAllShowsByProjectID($this->ProjectID);
        if (!(int) $this->ProjectID || !$allShows)
            return false;
        $videoPages = Versioned::get_by_stage('VideoOnDemandPage', 'Stage', "ParentID = '$this->ID'");
        foreach ($allShows as $show) {
            $flag = false;
            foreach ($videoPages as $video) {
                if (trim($show->ShowID) == trim($video->ShowID)) {
                    $video->StreamingFileURL = $show->StreamingFileURL;
                    $video->write();
                    if (trim($show->StreamingFileURL) == '') {
                        if ($video->isPublished())
                            $video->doUnpublish();
                    }
                    $flag = true;
                    break;
                }
            }      
            $checkpage = VideoOnDemandPage::get()->filter(array("ShowID" => $show->ShowID))->First();
            if (!$flag && !$checkpage) {
                $videoData = array(
                    'ParentID' => $this->ID,
                    'Title' => $show->Title,
                    'StreamingFileURL' => $show->StreamingFileURL,
                    'ShowID' => $show->ShowID,
                    'VideoType' => TIGHTROPE
                );
                $videoOndemandPage = new VideoOnDemandPage($videoData);
                $videoOndemandPage->writeToStage('Stage');
                if (trim($show->StreamingFileURL) != '') {
                   $videoOndemandPage->publish('Stage', 'Live');
                   $stage = Versioned::get_by_stage('VideoOnDemandPage', 'Stage', '`VideoOnDemandPage`.ID = ' . $videoOndemandPage->ID)->first();
                   $live = Versioned::get_by_stage('VideoOnDemandPage', 'Live', '`VideoOnDemandPage`.ID = ' . $videoOndemandPage->ID)->first();
                   $live->ShowID = $stage->ShowID;
                   $live->StreamingFileURL = $stage->StreamingFileURL;
                   $live->VideoType = $stage->VideoType;
                   $live->write();
                }
            }
        }
        if ($isImportFromCronjob) {
            $this->doCreatedArchivePage();
        }
    }

}

class TVProgramPage_Controller extends Page_Controller {
    
}

?>
