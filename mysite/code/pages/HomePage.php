<?php

class HomePage extends Page {

    static $db = array(
        'FirstColumnHeadline' => 'Varchar(255)',
        'FirstColumnContent' => 'HTMLText',
        'FirstColumnButtonText' => 'Varchar(255)',
        'FirstColumnButtonLink' => 'Text',
        'SecondColumnHeadline' => 'Varchar(255)',
        'SecondColumnContent' => 'HTMLText',
        'SecondColumnButtonText' => 'Varchar(255)',
        'SecondColumnButtonLink' => 'Text',
        'ThirdColumnHeadline' => 'Varchar(255)',
        'ThirdColumnContent' => 'HTMLText',
        'ThirdColumnButtonText' => 'Varchar(255)',
        'ThirdColumnButtonLink' => 'Text',
        'SponsorHeadline' => 'Varchar(255)',
        'SponsorContent' => 'HTMLText',
        'FirstSlideTitle' => 'Varchar(255)',
        'FirstSlideContent' => 'HTMLText',
        'FirstSlideButtonText' => 'Varchar(255)',
        'FirstSlideButtonLink' => 'Text',
        'SecondSlideTitle' => 'Varchar(255)',
        'SecondSlideContent' => 'HTMLText',
        'SecondSlideButtonText' => 'Varchar(255)',
        'SecondSlideButtonLink' => 'Text',
        'ThirdSlideTitle' => 'Varchar(255)',
        'ThirdSlideContent' => 'HTMLText',
        'ThirdSlideButtonText' => 'Varchar(255)',
        'ThirdSlideButtonLink' => 'Text',
        'TwitterSearch' => 'HTMLText',
        'WatchNowLink' => 'Text',
        'ShowWatchNowNewWindow' => 'Boolean',
        'ListenNowLink' => 'Text',
        'ShowListenNewWindow' => 'Boolean',
    );
    static $has_one = array(
        'WatchNowImage' => 'Image',
        'ListenNowImage' => 'Image'
    );
    
    static $has_many = array(
        'HomeSliders'   => 'HomeSlider'
    );
    
    static $defaults = array(
        'RotateEvery' => 5000, //rotate every 5 seconds
        'FirstColumnButtonLink' => '#',
        'SecondColumnButtonLink' => '#',
        'ThirdColumnButtonLink' => '#'
    );

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->findOrMakeTab('Root.MiddleBlock', 'Middle Section');     
        $fields->addFieldsToTab('Root.MiddleBlock', 
                new TabSet('MiddleBlockSet',
                        new Tab('Buttons',
                            new UploadField('WatchNowImage', 'Watch Now Image(385x102)'),
                            new TextField('WatchNowLink', 'Watch Now Link'),
                            new CheckboxField('ShowWatchNowNewWindow', 'Open popup?'),
                            new UploadField('ListenNowImage', 'Listen Now Image(385x102)'),                            
                            new TextField('ListenNowLink', 'Listen Now Link'),
                            new CheckboxField('ShowListenNewWindow', 'Open popup?')
                        ),
                        new Tab('First column', 
                                new HeaderField('FirstColumn', 'First column'), 
                                new TextField('FirstColumnHeadline', 'Title'), 
                                new HtmlEditorField('FirstColumnContent', 'Content'), 
                                new TextField('FirstColumnButtonText', 'Button'), 
                                new TreeDropdownField('FirstColumnButtonLink', 'Link', 'SiteTree', 'URLSegment', 'Title')
                                ), 
                        new Tab('Second column', 
                                new HeaderField('SecondColumn', 'Second column'), 
                                new TextField('SecondColumnHeadline', 'Title'), 
                                new HtmlEditorField('SecondColumnContent', 'Content'), 
                                new TextField('SecondColumnButtonText', 'Button'), 
                                new TreeDropdownField('SecondColumnButtonLink', 'Link', 'SiteTree', 'URLSegment', 'Title')
                                ), 
                        new Tab('Third column', 
                                new HeaderField('ThirdColumn', 'Third column'), 
                                new TextField('ThirdColumnHeadline', 'Title'), 
                                new HtmlEditorField('ThirdColumnContent', 'Content'), 
                                new TextField('ThirdColumnButtonText', 'Button'), 
                                new TreeDropdownField('ThirdColumnButtonLink', 'Link', 'SiteTree', 'URLSegment', 'Title')
                                )
                )
        );

        $fields->findOrMakeTab('Root.SponsorsBlock', 'Sponsors');
        $fields->addFieldsToTab('Root.SponsorsBlock', array(
            new HeaderField('Sponsors'),
            new TextField('SponsorHeadline', 'Title'),
            new HtmlEditorField('SponsorContent', 'Content')
        ));
        
        $conf = GridFieldConfig_RecordEditor::create(10);
        $conf->addComponent(new GridFieldOrderableRows('SortOrder'));
        
        $sliderGF = new GridField('HomeSliders', 'HomeSlider', $this->HomeSliders(), $conf);
        $fields->findOrMakeTab('Root.Slider', 'Slider');
        $fields->addFieldToTab('Root.Slider', $sliderGF);
        $fields->findOrMakeTab('Root.Twitter', 'Twitter');
        $fields->addFieldToTab('Root.Twitter', new TextareaField('TwitterSearch', "Twitter search"));
        return $fields;
    }

}

class HomePage_Controller extends Page_Controller {
    
}

?>
