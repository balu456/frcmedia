<?php

class RadioEpisodePage extends Page
{

    static $db = array(
        'ShowEmbedImage' => 'Boolean(false)'
    );
    static $has_one = array(
        'Image' => 'Image',
        'File' => 'File'
    );
    static $defaults = array();
    static $widgets_default = array(
        'FRCTVChannelGuideWidget',
        'LiveStreamingVideoWidget',
        'ConstantContactWidget',
        'JoinClassWidget',
    );

    function onAfterWrite()
    {
        parent::onAfterWrite();
        if (!$this->SideBarID) {
            $widgetarea = new WidgetArea();
            $widgetarea->write();
            $this->SideBarID = $widgetarea->ID;
            $widgets = self::$widgets_default;
            if (!empty($widgets)) {
                foreach ($widgets as $widget) {
                    $widgetObj = new $widget();
                    $widgetObj->ParentID = $this->SideBarID;
                    $widgetObj->write();
                }
            }
            $this->write();
        }
    }

    function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Mp3', new TabSet('Mp3',
                new Tab('First column',
                    new UploadField('File', 'Mp3 file'),
                    new UploadField('Image', 'Thumbnail image(125x75)'),
                    new CheckboxField('ShowEmbedImage', 'Show embed image from Front-end')
                ))
        );
        return $fields;
    }

}

class RadioEpisodePage_Controller extends Page_Controller
{


}

?>
