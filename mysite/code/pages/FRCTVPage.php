<?php

class FRCTVPage extends Page {

    static $db = array(
        'ViewProgram' => 'HTMLText'
    );
    static $has_one = array(
        'Image' => 'Image'
    );
    static $defaults = array(
    );

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Main', array(
            new UploadField('Image', 'Image(247x174)')
                ), $before = 'Content');
        $fields->addFieldToTab('Root.Program', new HtmlEditorField('ViewProgram', 'Program description'));
        
        return $fields;
    }
    
    function AllPrograms() {
        $programs = TVProgramPage::get()->filter(array('ParentID' => $this->ID))->sort(array('Title'=>"ASC"));
        return $programs->sort('Title') ? $programs : false;
    }
}

class FRCTVPage_Controller extends Page_Controller {

}

?>
