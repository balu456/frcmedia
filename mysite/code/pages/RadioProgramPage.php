<?php

class RadioProgramPage extends Page {

    static $db = array(
        'FirstLine' => 'Varchar(255)'
    );
    static $has_one = array(
        'TopImage' => 'Image',
        'VideoThumbnail' => 'Image'
    );
    static $defaults = array(
    );
    static $allowed_children = array(
        'RadioEpisodePage',
        'RadioArchivePage'
    );
    static $widgets_default = array(
        'FRCTVChannelGuideWidget',
        'LiveStreamingRadioWidget',
        'ConstantContactWidget',
        'JoinClassWidget',
        'SponsorWidget',
    );

    function onAfterWrite() {
        parent::onAfterWrite();
        if (!$this->SideBarID) {
            $widgetarea = new WidgetArea();
            $widgetarea->write();
            $this->SideBarID = $widgetarea->ID;
            $widgets = self::$widgets_default;
            if (!empty($widgets)) {
                foreach ($widgets as $widget) {
                    $widgetObj = new $widget();
                    $widgetObj->ParentID = $this->SideBarID;
                    $widgetObj->write();
                }
            }
            $this->write();
        }

        //create one RadioArchivePage page
        if (!RadioArchivePage::get()->filter('ParentID', $this->ID)->first()) {
            $radioArchivePage = new RadioArchivePage();
            $radioArchivePage->Title = 'Radio Archive';
            $radioArchivePage->RadioProgramPageID = $this->ID;
            $radioArchivePage->ParentID = $this->ID;
            $radioArchivePage->write();
            $radioArchivePage->publish('Stage', 'Live');
        }
    }

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldToTab('Root.Main', new TextField('FirstLine', 'First line above image'), 'Content');
        $fields->addFieldToTab('Root.TopImage', new UploadField('TopImage', 'Top image(536x141)'));

        $fields->findOrMakeTab('Root.VideoThumbnail', 'Video Thumbnail');
        $fields->addFieldToTab('Root.VideoThumbnail', new UploadField('VideoThumbnail', 'Default video thumbnail image(125x75)'));
        return $fields;
    }

    function revertTopImage() {
        if ($this->TopImageID != 0) {
            return $this->TopImage()->getWidth() > 536 ? $this->TopImage()->SetWidth(536) : $this->TopImage();
        }
    }

    function RadioEpisodes() {
        return $this->Children()->filter('ClassName', 'RadioEpisodePage');
    }

    function ArchivePage() {
        return RadioArchivePage::get()->filter('RadioProgramPageID', $this->ID)->first();
    }

}

class RadioProgramPage_Controller extends Page_Controller {
    
}

?>
