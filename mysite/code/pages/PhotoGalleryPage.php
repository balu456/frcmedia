<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GalleryPage
 *
 * @author THANHTAN
 */
class PhotoGalleryPage extends Page {

        static $db = array(
        );
        static $has_one = array(
        );
        static $has_many = array(
            'Albums' => 'PhotoGalleryAlbum',
            'GalleryItems' => 'PhotoGalleryItem'
        );
        static $defaults = array(
        );

        function getCMSFields() {
                Session::set('GalleryPageID',  $this->ID);
                $fields = parent::getCMSFields();
                //Albums tab
                $fields->findOrMakeTab('Root.Albums');
                $config = GridFieldConfig_RecordEditor::create();
                $config->addComponent(new GridFieldBulkEditingTools());
                $albumGF = new GridField(
                                'Albums',
                                'Albums',
                                $this->Albums(),
                                $config
                );
                $fields->addFieldToTab('Root.Albums', $albumGF);

                return $fields;
        }

        public function CurrentAlbum() {
                $c = Controller::curr();
                if (isset($c->urlParams['ID'])) {
                        $param = Convert::raw2sql($c->urlParams['ID']);
                        $params = explode('-', $param);
                        $albums = $this->Albums()->filter(array('ID' => $params[0]));
                        return $albums ? $albums->First() : false;
                }
                return false;
        }

}

class PhotoGalleryPage_Controller extends Page_Controller {

        public function album() {
                Requirements::css('themes/default/css/jquery.lightbox-0.5.css');
                return $this->renderWith(array('PhotoGalleryPage_album', 'Page'));
        }

}
?>
