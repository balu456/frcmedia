<?php

class FRCRadioPage extends Page {

    static $db = array(
        'ViewProgram' => 'HTMLText'
    );
    static $has_one = array(
        'Image' => 'Image'
    );
    static $defaults = array(
    );

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Main', array(
            new UploadField('Image', 'Image(247x174)')
                ), $before = 'Content');
        $fields->addFieldToTab('Root.Program', new HtmlEditorField('ViewProgram', 'Program description'));
        
        return $fields;
    }

}

class FRCRadioPage_Controller extends Page_Controller {

    function AllPrograms() {
        $programs = RadioProgramPage::get()->filter(array('ParentID' => $this->ID))->sort(array('Title'=>"ASC"));
        return $programs ? $programs : false;
    }

}

?>
