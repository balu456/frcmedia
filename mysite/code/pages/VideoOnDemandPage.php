<?php

class VideoOnDemandPage extends Page {

        static $db = array(
            'VideoID' => 'Varchar(255)',
            'ShowID' => 'Int',
            'StreamingFileURL' => 'Text',
            'VideoType' => "Int"
        );
        static $has_one = array(
            'Image' => 'Image',
        );
        static $defaults = array(
        );
        static $widgets_default = array(
            'FRCTVChannelGuideWidget',
            'LiveStreamingVideoWidget',
            'ConstantContactWidget',
            'JoinClassWidget',
        );

        function onAfterWrite() {
                parent::onAfterWrite();
                if (!$this->SideBarID) {
                        $widgetarea = new WidgetArea();
                        $widgetarea->write();
                        $this->SideBarID = $widgetarea->ID;
                        $widgets = self::$widgets_default;
                        if (!empty($widgets)) {
                                foreach ($widgets as $widget) {
                                        $widgetObj = new $widget();
                                        $widgetObj->ParentID = $this->SideBarID;
                                        $widgetObj->write();
                                }
                        }
                        $this->write();
                }
        }

        function getCMSFields() {
                $fields = parent::getCMSFields();
                $fields->addFieldsToTab('Root.Video', new TabSet('Video',
                                new Tab('First column',
                                        new ReadonlyField('ShowID', 'Show ID (From Tightrope)'),
                                        new TextField('StreamingFileURL', 'Streaming URL'),
                                        new TextField('VideoID', 'Youtube/ Vimeo ID'),
                                        new OptionsetField('VideoType', 'Select video player:', array(
                                            TIGHTROPE => 'Tightrope',
                                            YOUTUBE => 'Youtube video',
                                            VIMEO => 'Vimeo video'
                                        ), $this->VideoType),
                                        new UploadField('Image', 'Thumbnail image(125x75)')
                        ))
                );
                return $fields;
        }

        function URL() {
                return Director::absoluteURL($this->Link());
        }

        function VideoURL() {
                if ($this->VideoType == VIMEO)
                        return 'http://vimeo.com/' . $this->VideoID;
                else if ($this->VideoType == YOUTUBE) {
                        return 'http://www.youtube.com/embed/' . $this->VideoID;
                };
                return $this->StreamingFileURL;
        }

        function Type(){
            return strtoupper(pathinfo($this->VideoURL(), PATHINFO_EXTENSION));
        }
}

class VideoOnDemandPage_Controller extends Page_Controller {
        
        
}

?>
