<?php
class ContactPage extends UserDefinedForm{
    static $db = array(
        'MapLink'   => 'Text'
    );
    static $has_one = array(
        'MapImage'  => 'Image'
    );
    
    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->findOrMakeTab('Root.MapConfig', 'Map Config');
        $fields->addFieldsToTab('Root.MapConfig', array(
            new UploadField('MapImage', 'Map image(303x236)'),
            new TextField('MapLink', 'Link')
        ));
        return $fields;
    }
}
class ContactPage_Controller extends UserDefinedForm_Controller{
    
}
?>
