<?php

class RadioArchivePage extends Page
{

    static $has_one = array(
        'RadioProgramPage' => 'RadioProgramPage'
    );
    static $defaults = array(
        'ShowInFooter' => 0,
        'ShowInMenus' => 0
    );

    function getCMSFields()
    {
        $fields = parent::getCMSFields();
        return $fields;
    }

}

class RadioArchivePage_Controller extends Page_Controller
{

}

?>
