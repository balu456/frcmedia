<?php

class Page extends SiteTree {

    private static $db = array(
        "ShowInFooter" => "Boolean",
        "CustomMenuTitle" => "Varchar(255)",
        "InheritSideBar" => "Boolean"
    );
    private static $has_one = array(
        "SideBar" => "WidgetArea",
        'RightImage' => 'Image',
        "SideBar" => "WidgetArea"
    );
    private static $defaults = array(
        "ShowInFooter" => 1,
        "InheritSideBar" => 1
    );

    function getSettingsFields() {
        $fields = parent::getSettingsFields();
        $fields->addFieldToTab('Root.Settings',
            new CheckboxField('ShowInFooter', 'Show in footer menu?'),
            'ShowInSearch');
        return $fields;
    }

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.RightColumn',
            array(
                new UploadField('RightImage', 'Right Icon(267x67)')
            ));
        if ($this->ParentID != 0) {
            $fields->addFieldToTab("Root.Widgets",
                new CheckboxField('InheritSideBar',
                    'Inherit sidebar from parent?',
                    $this->InheritSideBar));
        }
        $fields->addFieldToTab("Root.Widgets",
            new WidgetAreaEditor("SideBar"));
        $fields->addFieldsToTab('Root.Main',
            array(
                new TextField('CustomMenuTitle', 'Custom Menu Title')
            ), 'MenuTitle');
        $fields->addFieldToTab("Root.Widgets", new WidgetAreaEditor("SideBar"));
        
        return $fields;
    }

    function SideBarTemplate() {
        if ($this->InheritSideBar && $this->getParent()) {
            $parent = $this->Parent();
            while (method_exists($parent, 'SideBarTemplate') && $parent->InheritSideBar) {
                if ($parent->Parent()->class == 'SiteTree') {
                    break;
                }
                else
                    $parent = $parent->Parent();
            }
            return $parent->SideBar();
        } elseif ($this->SideBarID) {
            return WidgetArea::get()->filter(array(
                'ID' => $this->SideBarID))->first();
        }
    }

    function getSponsors($limit = null) {
        return DataObject::get('Sponsor', '', '', '', $limit);
    }

    function getTVPrograms($limit = null) {
        $programPage = DataObject::get_one('TVProgramPage');
        if ($programPage) {
            return DataObject::get('SiteTree',
                "ParentID = {$programPage->ID}",
                'ID DESC', '', $limit);
        }
        return null;
    }

    function getSocials($limit = null) {
        return DataObject::get('SocialItem', '', '', '', $limit);
    }

    function ChildrenOf($parentRef) {
        $page = SiteTree::get_by_link($parentRef);
        if ($page) {
            return Page::get()
                ->addFilter(array(
                    'ParentID' => $page->ID,
                    'ShowInFooter' => 1
                ));
        };
        return false;
    }

    function HtmlEntityDecode($text) {
        return html_entity_decode($text);
    }

    function MenuTitle() {
        return $this->CustomMenuTitle ? $this->CustomMenuTitle : $this->MenuTitle;
    }

    function baseLink() {
        return substr(Director::absoluteBaseURL(), 0, -1);
    }

}

class Page_Controller extends ContentController {

    private static $allowed_actions = array(
    );

    public function init() {
        parent::init();

        // Note: you should use SS template require tags inside your templates 
        // instead of putting Requirements calls here.  However these are 
        // included so that our older themes still work
        Requirements::themedCSS('reset');
        Requirements::themedCSS('layout'); 
        Requirements::themedCSS('typography'); 
        Requirements::themedCSS('form'); 
    }

    public function rssParser($url) {
        $rss = new RestfulService($url);
        $xml = $rss->request()->getBody();
        $result = $xml ? $rss->getValues($xml, "channel", "item") : false;
        $output = '';
        if ($result)
            foreach ($result as $key => $value) {
                $description = str_replace('&amp;quot;',
                    '&quot;', $value->description);
                $output .= '<li><a href="' . $value->link . '">' . $value->title . '</a><br />' . $description . '</li>';
            }
        return $output;
    }

    function getFooterMenus() {
        $footerMenu = DataObject::get('Page',
            "ShowInFooter = 1 AND ParentID = 0");
        return $footerMenu ? $footerMenu : false;
    }

    function getFRCTVGuide() {
        $schedulePage = FRCTVProgrammingSchedulePage::get()->First();
        return $this->customise(array(
            'Schedules' => API::TVSchedules(5,
                $date = false, $startAM = false,
                $endAM = true),
            'SchedulesLink' => $schedulePage ? $schedulePage->Link() : '#',
            'Date' => date('l, F jS')
        ))
            ->renderWith('ScheduleTV');
    }

    public function RadioGuide() {
        $result = GoogleCalendar::getEventsByDate(null, 5);
        $schedulePage = RadioProgrammingSchedulePage::get()->First();
        return $this->customise(
            array(
                'Entries' => $result,
                'Date' => date('l, F jS'),
                'AllScheduleLink' => $schedulePage ? $schedulePage->Link() : false
            ))
            ->renderWith('RadioGuide');
    }
	/*
    public function getNewsFeed(){

        $results = new ArrayList();
        $rss = new RestfulService('http://frcconnect.org/feed/');
        $xml = $rss->request()->getBody();
        $feed = $xml ? $rss->getValues($xml, "channel", "item") : false;

        $count = 0;
        foreach($feed as $item => $value){
            $feedDate = trim(reset(explode('+', $value->pubDate)));
            $feedDate = DateTime::createFromFormat('D, j M Y H:i:s', $feedDate);
            $formatedDate = $feedDate->format('F j');
            if($count >= 3){ break; } // IMPORT ONLY LAST 3
            //if($feedDate->format('F j Y') != date('F j Y')){ break; } // IMPORT ONLY NEWS IN THAT DAY
            $results->push(
                new ArrayData(
                    array(
                        'title'         => $value->title,
                        'date'          => $formatedDate,
                        'link'          => $value->link,
                        'description'   => $this->newsExcerpt($value->description),
                    )
                )
            );
            $count++;
        }

        return $this->customise(
            array(
                'Entries'   => $results,
                'Date'      => date('l, F jS'),
            )
        )->renderWith('NewsFeed');
    }
	*/
	
	public function getNewsFeed(){

        $results = new ArrayList();
        $rss = new RestfulService('http://www.frcmedianews.org/feed/');
        $xml = $rss->request()->getBody();
        $feed = $xml ? $rss->getValues($xml, "channel", "item") : false;

        $count = 0;
        foreach($feed as $item => $value){
            // $feedDate = trim(reset(explode('+', $value->pubDate)));
            $feedDate = DateTime::createFromFormat('j-M-Y', '15-Feb-2009');
            $formatedDate = $feedDate->format('F j');
            if($count >= 3){ break; } // IMPORT ONLY LAST 3
            //if($feedDate->format('F j Y') != date('F j Y')){ break; } // IMPORT ONLY NEWS IN THAT DAY
            $results->push(
                new ArrayData(
                    array(
                        'title'         => $value->title,
                        'date'          => $formatedDate,
                        'link'          => $value->link,
                        'description'   => $this->newsExcerpt($value->description),
                    )
                )
            );
            $count++;
        }

        return $this->customise(
            array(
                'Entries'   => $results,
                'Date'      => date('l, F jS'),
            )
        )->renderWith('NewsFeed');
    }

    function newsExcerpt($string){

        $string = preg_replace('/\s+|&[a-z]{1,6};/', ' ', trim(strip_tags(html_entity_decode($string))));
        $string = str_replace('&nbsp', '', $string);
        if(strlen($string) <= 240) { return $string; }
        $words = explode(' ', $string);
        unset($words[count($words)-1]);
        $string = implode(' ', $words);
        return $this->newsExcerpt($string);
    }

    function getRootPage($page = false) {
        $page = $page ? $page : $this;
        if ($page->ParentID != 0) {
            return $this->getRootPage($page->Parent());
        }
        else
            return $page;
    }

    function getSearchResultPage() {
        return DataObject::get_one('SearchResultsPage');
    }

    function whatOS() {
        $uagent = $_SERVER['HTTP_USER_AGENT'];
        $oses = array(
            'Win311' => 'Win16',
            'Win95' => '(Windows 95)|(Win95)|(Windows_95)',
            'WinME' => '(Windows 98)|(Win 9x 4.90)|(Windows ME)',
            'Win98' => '(Windows 98)|(Win98)',
            'Win2000' => '(Windows NT 5.0)|(Windows 2000)',
            'WinXP' => '(Windows NT 5.1)|(Windows XP)',
            'WinServer2003' => '(Windows NT 5.2)',
            'WinVista' => '(Windows NT 6.0)',
            'Windows 7' => '(Windows NT 6.1)',
            'Windows 8' => '(Windows NT 6.2)',
            'WinNT' => '(Windows NT 4.0)|(WinNT4.0)|(WinNT)|(Windows NT)',
            'OpenBSD' => 'OpenBSD',
            'SunOS' => 'SunOS',
            'Ubuntu' => 'Ubuntu',
            'Android' => 'Android',
            'Linux' => '(Linux)|(X11)',
            'iPhone' => 'iPhone',
            'iPad' => 'iPad',
            'MacOS' => '(Mac_PowerPC)|(Macintosh)',
            'QNX' => 'QNX',
            'BeOS' => 'BeOS',
            'OS2' => 'OS/2',
            'SearchBot' => '(nuhk)|(Googlebot)|(Yammybot)|(Openbot)|(Slurp)|(MSNBot)|(Ask Jeeves/Teoma)|(ia_archiver)'
        );
        $uagent = strtolower($uagent ? $uagent : $_SERVER['HTTP_USER_AGENT']);
        foreach ($oses as $os => $pattern)
            if (preg_match('/' . $pattern . '/i', $uagent))
                return $os;
        return 'Unknown';
    }

}