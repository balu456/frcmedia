<?php

class AboutPage extends Page {

        static $db = array(
            'MissionHeadline' => 'Varchar(255)',
            'MissionContent' => 'HTMLText',
            'HoursOfOperationHeadline' => 'Varchar(255)',
            'HoursOfOperationContent' => 'HTMLText',
        );
        static $has_one = array(
            'Image' => 'Image'
        );
        static $defaults = array(
        );

        function getCMSFields() {
                $fields = parent::getCMSFields();
                $fields->findOrMakeTab('Root.MissionStatement', 'Mission Statement');
                $fields->addFieldsToTab('Root.Main', array(
                    new UploadField('Image', 'Image(247x182)')
                ),'Content');
                $fields->addFieldsToTab('Root.MissionStatement', array(
                    new TextField('MissionHeadline', 'Title'),
                    new HtmlEditorField('MissionContent', 'Content')
                ));

                $fields->findOrMakeTab('Root.HoursOfOperation', 'Hours Of Operation');
                $fields->addFieldsToTab('Root.HoursOfOperation', array(
                    new TextField('HoursOfOperationHeadline', 'Title'),
                    new HtmlEditorField('HoursOfOperationContent', 'Content')
                ));
                return $fields;
        }

}

class AboutPage_Controller extends Page_Controller {
        
}

?>
