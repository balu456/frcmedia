<?php

class ConnectPage extends Page {

        static $db = array(
            'ShowCommunity' => 'Boolean',
            'CommunityCalendarContent' => 'HTMLText',
            
            'ShowBulletinBoard' => 'Boolean',
            'BulletinBoardContent'    => 'HTMLText',
            
            'ShowTwitter' => 'Boolean',
            'TwitterContent' => 'HTMLText',
            
            'ShowFacebook' => 'Boolean',
            'FacebookContent' => 'HTMLText',
            
            'ShowYoutube' => 'Boolean',
            'YoutubeContent' => 'HTMLText',
            'YoutubeEmbedCode' => 'HTMLText',
        );
        static $has_one = array(
          
        );
        static $defaults = array(
        );

        function getCMSFields() {
                $fields = parent::getCMSFields();
                $communityCalendar = new HtmlEditorField('CommunityCalendarContent', 'Community Calendar Content');
                $bulletinBoardContent = new HtmlEditorField('BulletinBoardContent', 'Bulletin Board Content');
                $twitterField = new HtmlEditorField('TwitterContent', 'Twitter Content');
                $facebookField = new HtmlEditorField('FacebookContent', 'Facebook Content');
                $youtubeField = new HtmlEditorField('YoutubeContent', 'Youtube Content');
                
                $communityCalendar->addExtraClass('stacked');
                
                $bulletinBoardContent->addExtraClass('stacked');
                $twitterField->addExtraClass('stacked');
                $facebookField->addExtraClass('stacked');
                $youtubeField->addExtraClass('stacked');
                
                
                $fields->addFieldsToTab('Root.Community', array(
                    new CheckboxField('ShowCommunity', 'Show this block ?'),
                    $communityCalendar
                    ));
                $fields->addFieldsToTab('Root.Bulletin', array(
                    new CheckboxField('ShowBulletinBoard', 'Show this block ?'),
                    $bulletinBoardContent
                    ));
                $fields->addFieldsToTab('Root.Twitter', array(
                    new CheckboxField('ShowTwitter', 'Show this block ?'),
                    $twitterField
                    ));
                $fields->addFieldsToTab('Root.Facebook', array(
                    new CheckboxField('ShowFacebook', 'Show this block ?'),
                    $facebookField));
                $fields->addFieldsToTab('Root.Youtube', array(
                    new CheckboxField('ShowYoutube', 'Show this block ?'),
                    new TextareaField('YoutubeEmbedCode', 'Youtube playlist embed code'),
                    $youtubeField
                ));
                
                return $fields;
        }

}

class ConnectPage_Controller extends Page_Controller {
        
}

?>
