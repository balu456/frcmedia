<?php

class VideoArchivePage extends Page {

        static $has_one = array(
            'TVProgramPage' => 'TVProgramPage'
        );
        static $defaults = array(
            'ShowInFooter' => 0,
            'ShowInMenus' => 0
        );

        function getCMSFields() {
                $fields = parent::getCMSFields();
                return $fields;
        }

}

class VideoArchivePage_Controller extends Page_Controller {
        
}

?>
