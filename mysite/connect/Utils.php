<?php

class Utils {

        static function Time($Time, $Abbreviation = false) {
                if (!$Time) {
                        return false;
                }
                $format = $Abbreviation ? "g:i A" : "g:i";
                $date = new DateTime($Time);
                return $date->Format($format);
        }
        

}

?>
