<?php

class GoogleCalendarRSS extends RestfulService {
//        public function getValues($xml, $collection = NULL, $element = NULL) {
//                $xml = new SimpleXMLElement($xml);
//                $output = new ArrayList();
//
//                $childElements = $xml;
//                if ($collection)
//                        $childElements = $xml->{$collection};
//                if ($element)
//                        $childElements = $xml->{$collection}->{$element};
//
//                if ($childElements) {
//                        foreach ($childElements as $child) {
//                                $data = array(
//                                        );
//                                $whenAttribute = $child->xpath("gd:when");
//                                if ($whenAttribute) {
//                                        $whenAttribute = $whenAttribute[0]->attributes();
//                                        $data['startTime'] = Utils::Time($whenAttribute->startTime);
//                                        $data['endTime'] = Utils::Time($whenAttribute->endTime,
//                                                        true);
//                                }
//                                $this->getRecurseValues($child, $data);
//                                $output->push(new ArrayData($data));
//                        }
//                }
//                return $output;
//        }
//
//        protected function getRecurseValues($xml, &$data, $parent = "") {
//                $conv_value = "";
//                $child_count = 0;
//                foreach ($xml as $key => $value) {
//                        $child_count++;
//                        $k = ($parent == "") ? (string) $key : $parent . "_" . (string) $key;
//                        if ($this->getRecurseValues($value, $data, $k) == 0) {  // no childern, aka "leaf node"
//                                $conv_value = Convert::raw2xml($value);
//                        }
//                        //Review the fix for similar node names overriding it's predecessor
//                        if (array_key_exists($k, $data) == true) {
//                                $data[$k] = $data[$k] . "," . $conv_value;
//                        } else {
//                                $data[$k] = $conv_value;
//                        }
//                }
//                return $child_count;
//        }
}

class GoogleCalendar {

        //static $radioGuideURL = 'https://www.google.com/calendar/feeds/fallriverradio%40gmail.com/public';
        static $radioGuideURL = 'https://www.google.com/calendar/feeds/fallriverradio%40gmail.com/public/basic';
        static $detail_v3_api_url = 'https://www.googleapis.com/calendar/v3/calendars/fallriverradio@gmail.com/events/';
        static $ips_api_key = 'AIzaSyC9BmxxISMIB-Dk_c0oMkvZV1DMGJ0vQgw';
        static $domains_api_key = 'AIzaSyAV8hAjCOyBwwYMauq2k5T0jgoZVUH7Llk';
        /**
         * 
         * @param type $url
         * @return ArrayList|boolean
         */
        private static function parser($url) {
                $rss = new RestfulService($url);
                if ($rss->request()->isError())//check is valid input url
                        return false;
                $xml = $rss->request()->getBody();
                $result = @simplexml_load_string($xml) ? $rss->getValues($xml, "entry") : false;
                if ($result) {                        
                        foreach ($result as $value) {
                            $ID_URL = str_replace('http://www.google.com/calendar/feeds/', '', $value->id);
                            $id_arr = explode('/', $ID_URL);
                            $pos = count($id_arr) - 1;
                            $event_id = isset($id_arr[$pos]) ? $id_arr[$pos] : null;
                            if($event_id){
                                $detail_url = self::$detail_v3_api_url . $event_id . '?key=' . self::$ips_api_key;
                                $rss = new RestfulService($detail_url);
                                $xml = $rss->request()->getBody();
                                $detail_arr = Convert::json2array($xml);
                                if (isset($detail_arr['start'])) {
                                        $value->startTimeAM = Utils::Time($detail_arr['start']['dateTime'],
                                                        true);
                                        $value->startTime = Utils::Time($detail_arr['start']['dateTime']);
                                } else {
                                        $value->startTimeAM = 'Unknown';
                                }
                                if (isset($detail_arr['end'])) {
                                        $value->endTime = Utils::Time($detail_arr['end']['dateTime'], true);
                                } else {
                                        $value->endTime = 'Unknown';
                                }
                            }
                        }
                        return $result;
                }
                return false;
        }

        /**
         * 
         * @param type $limit
         * @return ArrayList
         */
        public static function getLatestEvents($limit = 5) {
                $dateString = date("Y-m-d", strtotime("-1 week"));
                $url = self::$radioGuideURL . "/full/?singleevents=true&start-min={$dateString}T00:00:00&max-results={$limit}&sortorder=a&orderby=starttime";
                return self::parser($url);
        }

        /**
         * 
         * @param type $date
         * @param type $limit
         * @return ArrayList
         */
        public static function getEventsByDate($date = null, $limit = null) {
                $dateString = $date ? $date : date("Y-m-d");
                $url = self::$radioGuideURL . "/?singleevents=true&start-min={$dateString}T00:00:00-08:00&start-max={$dateString}T23:59:59-08:00&sortorder=a&orderby=starttime";
                $result = self::parser($url);
                return $result ? $result->limit($limit) : false;
        }

}