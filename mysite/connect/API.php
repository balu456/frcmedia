<?php

class API {

        static $API_URL = "http://75.147.28.178/cablecastWS/CablecastWS.asmx?WSDL";
        static $scheduleDetailURL = 'http://75.147.28.178/Cablecast/Public/Show.aspx';
        static $scheduleTVURL = 'http://75.147.28.178/Cablecast/Public/Main.aspx&ChannelID=1';
        static $ChannelID = 1;

        function __construct() {
                
        }

        static function get7daysTV() {
                $today = date('j/n/Y');
                return "http://75.147.28.178/Cablecast/Plugins/RSSSchedule/rss.aspx?StartDate=" . $today . "&Days=7&ChannelID=1";
        }

        /**
         *
         * @return \ArrayList 
         */
        static function TVSchedules($limit = false, $date = false, $startAM = false, $endAM = false) {
                $client = new SoapClient(self::$API_URL);
                $dateString = $date ? $date : date("Y-m-d");
                $channelID = 1;
                $result = $client->GetScheduleInformation(
                        array(
                            'ChannelID' => $channelID,
                            'FromDate' => $dateString . "T00:00:00",
                            'ToDate' => $dateString . "T23:59:59",
                            'restrictToShowID' => 0)
                );
                if ($result->GetScheduleInformationResult->ScheduleInfo == NULL) {
                        $scheduleItems = array();
                } else {
                        $scheduleItems = is_array($result->GetScheduleInformationResult->ScheduleInfo) ?
                                $result->GetScheduleInformationResult->ScheduleInfo :
                                array($result->GetScheduleInformationResult->ScheduleInfo);
                }
                $returnSet = new ArrayList();
                $now = date('H:i:s');
                $getOffset = false;
                $offset = 0;
                foreach ($scheduleItems as $item) {
                        $checkTimeArr = explode('T', $item->StartTime);
                        $nextDay = date('Y-m-d', strtotime($dateString . ' +1days'));
                        if ($checkTimeArr[0] == $nextDay)
                                continue;
                        $checkTime = $checkTimeArr[1];
                        if (!$getOffset) {
                                if (strtotime($now) < strtotime($checkTime))
                                        $getOffset = true;
                                else
                                        $offset++;
                        }
                        $item->Start = Utils::Time($item->StartTime, $startAM);
                        $item->End = Utils::Time($item->EndTime, $endAM);
                        $item->Link = self::$scheduleDetailURL . "?ChannelID=$channelID&ShowID={$item->ShowID}";
                        $returnSet->push(new ArrayData($item));
                }
                if ($limit)
                        return $returnSet->limit($limit, ($offset - 1));
                else
                        return $returnSet;
        }

        /**
         * Get all video on demand by project ID
         * @param int $projectID
         * @return \ArrayList|boolean 
         */
        static function getAllShowsByProjectID($projectID) {
                $client = new SoapClient(self::$API_URL);
                $result = $client->AdvancedShowSearch(array(
                    'ChannelID' => self::$ChannelID,
                    'searchString' => '',
                    'eventDate' => date("Y-m-d") . "T00:00:00",
                    'dateComparator' => '',
                    'restrictToCategoryID' => 0,
                    'restrictToProducerID' => 0,
                    'restrictToProjectID' => $projectID,
                    'displayStreamingShowsOnly' => 0,
                    'searchOtherSites' => 0,
                        ));
                //print_r($result);die;
                if (!isset($result->AdvancedShowSearchResult->SiteSearchResult->Shows->ShowInfo)) {
                        $vods = array();
                } else {
                        $vods = is_array($result->AdvancedShowSearchResult->SiteSearchResult->Shows->ShowInfo) ?
                                $result->AdvancedShowSearchResult->SiteSearchResult->Shows->ShowInfo :
                                array($result->AdvancedShowSearchResult->SiteSearchResult->Shows->ShowInfo);
                }
                $returnSet = new ArrayList();
                if (!empty($vods)) {
                        foreach ($vods as $vod) {
                                $returnSet->push(new ArrayData($vod));
                        }
                        return $returnSet;
                }
                return false;
        }

        static function getDigitalFiles($localID) {
                $client = new SoapClient(self::$API_URL);
                $result = $client->AdvancedShowSearch(array(
                    'LocationID' => $localID,
                    'IncludeValidFiles' => 0,
                    'IncludeInvalidFiles' => 0,
                    'RestrictToOrphanedFiles' => 0,
                    'RestrictToShowID' => 0
                        ));

                if (!isset($result->GetDigitalFilesResult)) {
                        $files = array();
                } else {
                        $files = is_array($result->GetDigitalFilesResult) ?
                                $result->GetDigitalFilesResult :
                                array($result->GetDigitalFilesResult);
                }
                $returnSet = new ArrayList();
                if (!empty($files)) {
                        foreach ($files as $file) {
                                $returnSet->push(new ArrayData($file));
                        }
                        return $returnSet;
                }
                return false;
        }

        /**
         * Get all projects on ChannelID = 1
         * @return \ArrayList 
         * Eg: 
         * Array
          (
         * [0] => stdClass Object
         * ****     (
         * ****[ProjectID] => 121
         * ****[Name] => Master Builders Ministries
         * ****[Description] => Focusing on Hope. Learn How you can be strengthened to strengthen your family and community
         * ****[HasPodcast] =>
         * )
          )
         * 
         * 
         */
        static function getAllProjects() {
                $client = new SoapClient(self::$API_URL);
                $result = $client->GetProjects(array(
                    'ChannelID' => self::$ChannelID,
                        ));
                if (!isset($result->GetProjectsResult->Project)) {
                        $projects = array();
                } else {
                        $projects = is_array($result->GetProjectsResult->Project) ?
                                $result->GetProjectsResult->Project :
                                array($result->GetProjectsResult->Project);
                }
                return new ArrayList($projects);
        }

        /**
         *
         * @param Int $projectID
         * @return \ArrayData|boolean 
         */
       static function getProjectByID($projectID){
               $projects = self::getAllProjects();
               if($projects){
                       foreach ($projects as $project) {
                               if($project->ProjectID == $projectID){
                                       return new ArrayData($project);
                               }
                       }
               }
               return false;
       }
}

?>
