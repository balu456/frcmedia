<% if ClassName != HomePage %>
<div class="MainRight">
    <% if RightImage %>
        <div class="frc-connect-logo">
            <img src="$RightImage.URL" width="267" height="67" alt="" />
        </div>
    <% else %>
        <% with getRootPage %>
            <% if RightImage %>
                <div class="frc-connect-logo">
                    <img src="$RightImage.URL" width="267" height="67" alt="" />
                </div>
            <% end_if %>
        <% end_with %>
    <% end_if %>
    $SideBarTemplate
</div>
<% else_if Query %>
    <div class="MainRight">
        $SideBarTemplate
    </div>
<% end_if %>