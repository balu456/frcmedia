<div class="title">$Date</div>
<% if Schedules %>
<ul>
    <% loop Schedules %>
    <li class="clearfix"><a>$ShowTitle</a><span>$Start - $End</span></li>
    <% end_loop %>
</ul>
<% end_if %>
<div class="link"><a href="$SchedulesLink">View full Schedule &gt;</a></div>