<li>
    <div>
        <div class="title clearfix"><img src="$ThemeDir/images/icon-slider-watch.png" width="60" height="40" />
            <h2>$LiveStreamingVideoTitle</h2>
        </div>
        <div class="textSlide">
            <div class="rightside">
                <% if UseEmbedVideo %>
                $EmbedVideo
                <% else %>
                <% end_if %>
            </div>
            <div class="slider-content">
                <div class="blockText">
                    <p>$LiveStreamingVideoDesc</p>
                </div>
                <div class="slider-link"><a href="$LiveStreamingVideoLink">$LiveStreamingVideoButtonLink</a></div>
            </div>
        </div>
    </div>
</li>