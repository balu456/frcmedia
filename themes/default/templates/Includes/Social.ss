<% if getSocials %>
<div class="social">
            <span>CONNECT</span>
            <% loop getSocials %>
                <a href="$Link" target="_blank">$Icon.CroppedImage(30,30)</a>
            <% end_loop %>
    </div>
<% end_if %>