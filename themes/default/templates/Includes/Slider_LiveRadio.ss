<li>
    <div>
        <div class="title clearfix"><img src="$ThemeDir/images/icon-slider-node.png" width="60" height="40" />
            <h2>$LiveStreamingRadioTitle</h2>
        </div>
        <div class="textSlide">
            <div class="rightside">
                <iframe width="100%" height="271" scrolling="no" frameborder="no" src="{$Top.baseLink}/radio365/popup"></iframe>
            </div>
            <div class="slider-content">
                <div class="blockText">
                    <p>$LiveStreamingRadioDesc</p>
                </div>
                <div class="slider-link"><a href="$LiveStreamingRadioLink">$LiveStreamingRadioButtonLink</a></div>
            </div>
        </div>
    </div>
</li>