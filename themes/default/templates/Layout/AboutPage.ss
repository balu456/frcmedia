<div class="MainLeft">
    <div class="page-title">
        <h2>$Title</h2>
    </div>
    <div class="about-content clearfix"> 
        <% if Image %><img class="right" src="<% with $Image %>$CroppedImage(247,182).URL<% end_with %>" width="247" height="182" /><% end_if %>
        $Content
    </div>
    <div class="about-mission">
        <h5>$MissionHeadline</h5>
        <div class="mission-content">$MissionContent</div>
        <h5>$HoursOfOperationHeadline</h5>
        <div class="mission-hour">
            $HoursOfOperationContent
        </div>
    </div>
</div>