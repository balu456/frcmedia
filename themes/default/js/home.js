jQuery(function($) {
    /* Homepage - Top slider */
    var scPlaying = [];
    if($('#slider1 li').length > 0){
        $('#slider1').anythingSlider({
            hashTags: false,
            theme: 'metallic',
            easing: 'easeInOutBack',
            resizeContents: true,
            addWmodeToObject: 'transparent',
            buildArrows: true,
            buildNavigation: false,
            buildStartStop: false,
            enableNavigation: false,
            enableStartStop: false,
            enableKeyboard: false,
            autoPlay: true,
            autoPlayLocked: true,
            autoPlayDelayed: true,
            delay: 6000,
            resumeDelay: 3600,
            animationTime: 600,
            delayBeforeAnimate: 0
        });
    }

    /* Home - Tab control */
    $(".tab_content").hide();
    $(".tab_content:first").show();

    $("ul.tabs li").click(function() {
        $("ul.tabs li").removeClass("active");
        $(this).addClass("active");
        $(".tab_content").hide();
        var activeTab = $(this).attr("rel");
        $("#" + activeTab).fadeIn();
    });

    $('#spon').anythingSlider({
        hashTags: false,
        buildArrows: false,
        buildNavigation: false,
        buildStartStop: false,
        enableArrows: false,
        enableNavigation: false,
        enableStartStop: false,
        enableKeyboard: false,
        autoPlay: true,
        autoPlayLocked: true,
        autoPlayDelayed: true,
        delay: 5000,
        resumeDelay: 5000,
        animationTime: 600,
        delayBeforeAnimate: 0
    });
});