jQuery(function($) {
    $(document).ready(function() {
        /* Header - Search focus */
        var clearMePrevious = 'Search FRCMedia.org';
        // clear input on focus
        $('#SearchForm_SearchForm_Search').focus(function() {
            if ($(this).val() == clearMePrevious) {
                clearMePrevious = $(this).val();
                $(this).val('');
            }
        });
        // if field is empty afterward, add text again
        $('#SearchForm_SearchForm_Search').blur(function() {
            if ($(this).val() == '') {
                $(this).val(clearMePrevious);
            }
        });
        
        $('#SearchForm_SearchForm button').click(function(){
            if($('#SearchForm_SearchForm_Search').val() == clearMePrevious){
                    return false;
            }  
            $('#SearchForm_SearchForm').submit();
        });
    })
}); 