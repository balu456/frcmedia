<?php
/**
 * Tests for the {@link GridFieldOrderableRows} component.
 */
class GridFieldOrderableRowsTest extends SapphireTest {

	protected $usesDatabase = true;

	/**
	 * @covers GridFieldOrderableRows::getSortTable
	 */
	public function testGetSortTable() {
		$orderable = new GridFieldOrderableRows();

		$parent = new GridFieldOrderableRowsTest_Parent();
		$parent->write();

		$this->assertEquals(
			'GridFieldOrderableRowsTest_Ordered',
			$orderable->getSortTable($parent->HasMany())
		);

		$this->assertEquals(
			'GridFieldOrderableRowsTest_Ordered',
			$orderable->getSortTable($parent->HasManySubclass())
		);

		$this->assertEquals(
			'GridFieldOrderableRowsTest_Ordered',
			$orderable->getSortTable($parent->ManyMany())
		);

		$this->assertEquals(
			'GridFieldOrderableRowsTest_Parent_ManyMany',
			$orderable->setSortField('ManyManySort')->getSortTable($parent->ManyMany())
		);
	}

}

/**#@+
 * @ignore
 */

class GridFieldOrderableRowsTest_Parent extends DataObject {

	public  static $has_many = array(
		'HasMany' => 'GridFieldOrderableRowsTest_Ordered',
		'HasManySubclass' => 'GridFieldOrderableRowsTest_Subclass'
	);

	public  static $many_many = array(
		'ManyMany' => 'GridFieldOrderableRowsTest_Ordered'
	);

	public  static $many_many_extraFields = array(
		'ManyMany' => array('ManyManySort' => 'Int')
	);

}

class GridFieldOrderableRowsTest_Ordered extends DataObject {

	public  static $db = array(
		'Sort' => 'Int'
	);

	public  static $has_one = array(
		'Parent' => 'GridFieldOrderableRowsTest_Parent'
	);

}

class GridFieldOrderableRowsTest_Subclass extends GridFieldOrderableRowsTest_Ordered {
}

/**#@-*/
